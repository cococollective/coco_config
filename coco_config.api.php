<?php

/**
 * @file
 * coco_config.api.php
 *
 * @todo Add better file description.
 */

/**
 * Provide field bases for Coco sites.
 *
 * @todo Add better documentation.
 *
 * @return array
 *   An array containing field bases.
 */
function hook_coco_config_default_field_bases() {
  $field_bases = array();
  // @todo Add example code.
  return $field_bases;
}

/**
 * Alter field bases for Coco sites.
 *
 * @todo Add better documentation.
 *
 * @param array $field_bases
 *   An array containing field bases passed by reference.
 *
 * @see hook_coco_config_default_vocabularies()
 */
function hook_coco_config_default_field_bases_alter(&$field_bases) {
  // @todo Add example code.
}

/**
 * Provide field instances for Coco sites.
 *
 * @todo Add better documentation.
 *
 * @return array
 *   An array containing field instances.
 */
function hook_coco_config_default_field_instances() {
  $field_instances = array();
  // @todo Add example code.
  return $field_instances;
}

/**
 * Alter field instances for Coco sites.
 *
 * @todo Add better documentation.
 *
 * @param array $field_instances
 *   An array containing field instances passed by reference.
 *
 * @see hook_coco_config_default_vocabularies()
 */
function hook_coco_config_default_field_instances_alter(&$field_instances) {
  // @todo Add example code.
}

/**
 * Provide roles for Coco sites.
 *
 * @todo Add better documentation.
 *
 * @return array
 *   An array containing roles.
 */
function hook_coco_config_default_roles() {
  $roles = array();
  // @todo Add example code.
  return $roles;
}

/**
 * Alter roles for Coco sites.
 *
 * @todo Add better documentation.
 *
 * @param array $roles
 *   An array containing roles passed by reference.
 *
 * @see hook_coco_config_default_vocabularies()
 */
function hook_coco_config_default_roles_alter(&$roles) {
  // @todo Add example code.
}

/**
 * Provide vocabularies for Coco sites.
 *
 * @todo Add better documentation.
 *
 * @return array
 *   An array containing vocabularies.
 */
function hook_coco_config_default_vocabularies() {
  $vocabularies = array();
  // @todo Add example code.
  return $vocabularies;
}

/**
 * Alter vocabularies for Coco sites.
 *
 * @todo Add better documentation.
 *
 * @param array $vocabularies
 *   An array containing vocabularies passed by reference.
 *
 * @see hook_coco_config_default_vocabularies()
 */
function hook_coco_config_default_vocabularies_alter(&$vocabularies) {
  // @todo Add example code.
}
