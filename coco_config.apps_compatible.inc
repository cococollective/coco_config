<?php
/**
 * @file
 * Apps Compatible hooks for coco_config.
 */

/**
 * Implements hook_apps_compatible_info().
 */
function coco_config_apps_compatible_info() {
  return array(
    // Ensure a set of roles is created.
    'role' => array(
      'administrator' => array(
        'machine name' => 'administrator',
      ),
      'contributor' => array(
        'machine name' => 'contributor',
      ),
      'editor' => array(
        'machine name' => 'editor',
      ),
      'manager' => array(
        'machine name' => 'manager',
      ),
      'member' => array(
        'machine name' => 'member',
      ),
    ),
    // Ensure the 'tags' vocabulary, as defined in apps_compatible, is created.
    'vocabulary' => array(
      'tags' => array(
        'machine name' => 'tags',
      ),
    ),
    // Ensure a set of field bases, as defined in apps_compatible, is created.
    'field_base' => array(
      'body' => array(
        'machine name' => 'body',
      ),
    ),
  );
}
