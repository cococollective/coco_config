<?php
/**
 * @file
 * coco_config.default_breakpoint_group.inc
 */

/**
 * Implements hook_default_breakpoint_group().
 */
function coco_config_default_breakpoint_group() {
  $export = array();

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'coco_teaser';
  $breakpoint_group->name = 'Coco Teaser';
  $breakpoint_group->breakpoints = array(
    0 => 'module.coco_teaser.lg-desktop',
    1 => 'module.coco_teaser.desktop',
    2 => 'module.coco_teaser.tablet',
    3 => 'module.coco_teaser.phone',
    4 => 'module.coco_teaser.sm-phone',
  );
  $breakpoint_group->type = 'theme';
  $breakpoint_group->overridden = 0;
  $export['coco_teaser'] = $breakpoint_group;

  return $export;
}
