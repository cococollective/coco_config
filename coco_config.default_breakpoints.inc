<?php
/**
 * @file
 * coco_config.default_breakpoints.inc
 */

/**
 * Implements hook_default_breakpoints().
 */
function coco_config_default_breakpoints() {
  $export = array();

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'module.coco_teaser.desktop';
  $breakpoint->name = 'desktop';
  $breakpoint->breakpoint = '(min-width: 992px)';
  $breakpoint->source = 'coco_teaser';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 1;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['module.coco_teaser.desktop'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'module.coco_teaser.lg-desktop';
  $breakpoint->name = 'lg-desktop';
  $breakpoint->breakpoint = 'only screen and (min-width: 1200px)';
  $breakpoint->source = 'coco_teaser';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 0;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['module.coco_teaser.lg-desktop'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'module.coco_teaser.phone';
  $breakpoint->name = 'phone';
  $breakpoint->breakpoint = '(min-width: 480px)';
  $breakpoint->source = 'coco_teaser';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 3;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['module.coco_teaser.phone'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'module.coco_teaser.sm-phone';
  $breakpoint->name = 'sm-phone';
  $breakpoint->breakpoint = '(min-width: 0px)';
  $breakpoint->source = 'coco_teaser';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 4;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['module.coco_teaser.sm-phone'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'module.coco_teaser.tablet';
  $breakpoint->name = 'tablet';
  $breakpoint->breakpoint = '(min-width: 768px)';
  $breakpoint->source = 'coco_teaser';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 2;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['module.coco_teaser.tablet'] = $breakpoint;

  return $export;
}
