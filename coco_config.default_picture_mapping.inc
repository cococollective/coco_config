<?php
/**
 * @file
 * coco_config.default_picture_mapping.inc
 */

/**
 * Implements hook_default_picture_mapping().
 */
function coco_config_default_picture_mapping() {
  $export = array();

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Coco Teaser';
  $picture_mapping->machine_name = 'coco_teaser';
  $picture_mapping->breakpoint_group = 'coco_teaser';
  $picture_mapping->mapping = array(
    'module.coco_teaser.lg-desktop' => array(
      '1x' => array(
        'mapping_type' => '_none',
      ),
    ),
    'module.coco_teaser.desktop' => array(
      '1x' => array(
        'mapping_type' => '_none',
      ),
    ),
    'module.coco_teaser.tablet' => array(
      '1x' => array(
        'mapping_type' => '_none',
      ),
    ),
    'module.coco_teaser.phone' => array(
      '1x' => array(
        'mapping_type' => '_none',
      ),
    ),
    'module.coco_teaser.sm-phone' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'medium',
      ),
    ),
  );
  $export['coco_teaser'] = $picture_mapping;

  return $export;
}
