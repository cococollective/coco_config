<?php
/**
 * @file
 * Provides features-related hook implementations by Coco Config.
 */

/**
 * Implements hook_features_export_alter().
 *
 * Remove the coco_config controlled vocabularies.
 */
function coco_config_features_export_alter(&$export, $module_name) {
  if (isset($export['features']['taxonomy']) && isset($export['features']['taxonomy']['tags'])) {
    unset($export['features']['taxonomy']['tags']);
  }
}
