<?php

/**
 * @file
 * Component specific code for Coco Config.
 */

/**
 * Finds the components provided by hook implementations.
 *
 * @param string $type
 *   (Optional) Documentation: coco_config_components()
 * @param bool $rebuild
 *   (Optional) Documentation: coco_config_components()
 *
 * @return array
 *   Documentation: coco_config_components()
 *
 * @see coco_config_components()
 */
function _coco_config_components($type = NULL, $rebuild = FALSE) {
  $components = &drupal_static(__FUNCTION__, array());;

  if (empty($components) || $rebuild) {
    $components = cache_get(__FUNCTION__);

    if ($components === FALSE || $rebuild) {
      module_load_include('inc', 'coco_config', 'includes/coco_config.components');
      $components = _coco_config_invoke_component_hooks();
      cache_set(__FUNCTION__, $components);
    }
  }

  $return = $components;
  // If a valid type is provided, return only the items of those types.
  if (is_string($type) && in_array($type, coco_config_component_types())) {
    $return[$type] = $components[$type];
  }

  return $return;
}

/**
 * Rebuilds the the components provided by hook implementations.
 *
 * @param array $modules
 *   (Optional) An array with the modules that need provide components that need
 *   to be rebuild.
 */
function coco_config_rebuild_components($modules = array()) {
  $valid_types = array();
  $callbacks = array();
  $components = coco_config_components(NULL, TRUE);

  foreach ($components as $type => $type_items) {
    // If we have modules, remove the components provided by other modules.
    if (!empty($modules)) {
      $all_components = $components[$type];
      $components[$type] = array();
      foreach ($modules as $module) {
        $components[$type][$module] = $all_components[$module];
      }
      unset($all_components);
    }

    // Get the info about callbacks and their files. Some component types
    // share callbacks so we filter out duplicate things here.
    if (!empty($components[$type])) {
      if (empty($valid_types)) {
        $valid_types = coco_config_component_types();
      }
      $file_name = $valid_types[$type]['file_name'];
      $callback = $valid_types[$type]['callback'];
      $callbacks[$file_name] = $callback;
    }
  }

  foreach ($callbacks as $file => $callback) {
    module_load_include('', 'coco_config', $file);
    call_user_func($callback);
  }
}

/**
 * Invokes the Coco Config component hooks.
 *
 * @return array
 *   An array containing sub-arrays keyed by type, with all component arrays.
 */
function _coco_config_invoke_component_hooks() {
  $components = array();
  $hook_prefix = 'coco_config_default_';
  $component_types = coco_config_component_types();

  foreach ($component_types as $type => $type_info) {
    // Hook name is coco_config_default_TYPE:
    $hook_name = $hook_prefix . $type;
    $components[$type] = array();
    $implementations = module_implements($hook_name);

    foreach ($implementations as $module) {
      $items = module_invoke($module, $hook_name);
      foreach ($items as $name => $item) {
        $components[$type][$module][$name] = $item;
      }
    }

    // Let other modules override the provided components.
    drupal_alter($hook_name, $components[$type]);
  }

  return $components;
}
