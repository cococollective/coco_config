<?php
/**
 * @file
 * coco_config.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function coco_config_taxonomy_default_vocabularies() {
  return array(
    'coco_topics' => array(
      'name' => 'Topics',
      'machine_name' => 'coco_topics',
      'description' => 'Use topics to group related content into categories.',
      'hierarchy' => 0,
      'module' => 'coco_config',
      'weight' => -9,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
